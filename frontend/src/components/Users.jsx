import { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

export default class Users extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: '',
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    console.log(event.target.value);
    return () => {
      this.setState({ user: event.target.value });
    };
  }

  render() {
    return (
      <Fragment>
        <Typography variant="h4" component="h2">Users</Typography>
        <Typography variant="h5" component="h3">Current Users</Typography>
        <ul>
          {this.props.profiles.map(profile => (
            <li>
              <Typography variant="subtitle1" component="li">
                {`${profile.user.first_name} ${profile.user.last_name}`}
              </Typography>
            </li>
          ))}
        </ul>
        <FormControl style={{ width: '20%' }}>
          <InputLabel id="demo-simple-select-label">Age</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={this.state.user}
            onChange={this.handleChange}
          >
            {this.props.users.map(user => (
              <MenuItem
                value={user.last_name}
                option={user.last_name}
              >
                {user.first_name} {user.last_name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Fragment>
    );
  }
}

Users.defaultProps = {
  profiles: null,
  users: null,
};

Users.propTypes = {
  profiles: PropTypes.arrayOf(PropTypes.shape({
    user: PropTypes.shape({
      first_name: PropTypes.string,
      last_name: PropTypes.string,
    }),
  })),
  users: PropTypes.arrayOf(PropTypes.shape({
    first_name: PropTypes.string,
    last_name: PropTypes.string,
  })),
};
